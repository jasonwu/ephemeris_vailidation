import git, sys, subprocess, shlex, os
#os.chdir("/builds/jasonwu/eff_psr_obs/")
try:
    os.mkdir("/builds/jasonwu/tmp/")
except:
    print("")
#print("creating new tmp directory")
os.chdir("/builds/jasonwu/tmp/eff_psr_obs")
#print("cd to tmp")
#try:
#    os.remove("/builds/jasonwu/tmp/eff_psr_obs")
#except: 
#    print("error")
#git.Repo.clone_from("https://gitlab.mpcdf.mpg.de/jasonwu/eff_psr_obs.git","/builds/jasonwu/tmp/eff_psr_obs")
repo = git.Repo("/builds/jasonwu/tmp/eff_psr_obs")
#repo = git.Repo("./")
repo.git.checkout("master")
repo.git.pull()
commits_list = list(repo.iter_commits())
repo.git.checkout("sanity_check")
repo.git.pull()
commits_list_sanity_check = list(repo.iter_commits())
changed_files=[]
for x in commits_list[0].diff(commits_list_sanity_check[0]):
    if x.a_path not in changed_files:
        changed_files.append(x.a_path)
    if x.b_blob is not None and x.a_path not in changed_files:
        changed_files.append(x.a_path)

print("Changed_files:")
for filename in changed_files:
    print(filename)      

for filename in changed_files:
    print("Working on {} ".format(filename))
    if filename.endswith(".par"):
        cmd = 'tempo2 -f {} -pred "Effelsberg 59455.8948567 59455.9948567 1200 1201 1 1 8640.0"'.format(filename)
        P= subprocess.call(shlex.split(cmd))
        if P==0:
            print("{} Passed".format(filename))
        else:
            print("{} Failed".format(filename))
            sys.exit(1)
    else:
        continue
